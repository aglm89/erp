package com.rest.bd.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.rest.bd.service.EmpleadosService;

@RestController
public class EmpleadosControllerImpl implements EmpleadosController{
	
	@Autowired
	EmpleadosService service;

	@Override
	public ResponseEntity<?> consultaEmpleados() {
		return service.consultaEmpleados();
	}

}
