package com.rest.bd.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

public interface EmpleadosController {
	
	@GetMapping(value="/empleados")
	public ResponseEntity<?> consultaEmpleados();

}
