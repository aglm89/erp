package com.rest.bd.service;

import org.springframework.http.ResponseEntity;

public interface EmpleadosService {

	public ResponseEntity<?> consultaEmpleados();
	
}
