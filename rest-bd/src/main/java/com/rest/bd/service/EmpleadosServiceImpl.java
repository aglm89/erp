package com.rest.bd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.rest.bd.Entities.Empleado;
import com.rest.bd.Entities.ResponseEmpleados;
import com.rest.bd.repository.EmpleadosRepository;

@Service
public class EmpleadosServiceImpl implements EmpleadosService {
	
	@Autowired
	EmpleadosRepository repo;

	@Override
	public ResponseEntity<?> consultaEmpleados() {
		List<Empleado> listadoEmpleados = (List<Empleado>) repo.findAll();
		if(!listadoEmpleados.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK).body(new ResponseEmpleados(listadoEmpleados, null));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseEmpleados(null, "SIN RESULTADOS"));
		}
	}

}
