package com.rest.bd.Entities;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseEmpleados implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private List<Empleado> Empleados;
	
	private String error;

}
