package com.rest.bd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class RestBdApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestBdApplication.class, args);
	}

}
