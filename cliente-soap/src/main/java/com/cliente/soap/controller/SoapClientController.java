package com.cliente.soap.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cliente.soap.Entities.RequestResolveIp;
import com.cliente.soap.service.SoapClient;

@RestController
public class SoapClientController {
	
	@Autowired
	SoapClient soapClient;
	
	@PostMapping(value = "/resolve-ip")
	public ResponseEntity<?> ResolveIP(@RequestBody RequestResolveIp rq){
		return soapClient.ResolveIP(rq);
	}

}
