package com.cliente.soap.service;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.cdyne.ws.ResolveIP;
import com.cdyne.ws.ResolveIPResponse;
import com.cliente.soap.Entities.RequestResolveIp;
import com.cliente.soap.Entities.ResolveIPResponseRest;
import com.cliente.soap.headers.SoapHeaders;

@Service
@Component("ip2geo.asmx")
public class SoapClient extends WebServiceGatewaySupport {
	
	private String endpoint = "http://ws.cdyne.com/ip2geo/ip2geo.asmx";
	
	public ResponseEntity<?> ResolveIP(RequestResolveIp rq){
		
		System.out.println(rq.toString());
		
		ResolveIP request = new ResolveIP(); 
		request.setIpAddress(rq.getIpAddress());
		request.setLicenseKey(rq.getLicenseKey());
		
		ResolveIPResponse responsews = (ResolveIPResponse) getWebServiceTemplate().marshalSendAndReceive(endpoint, request, new SoapHeaders());
		
		ResolveIPResponseRest response = new ResolveIPResponseRest();
		
		BeanUtils.copyProperties(responsews, response);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
		
	}

}
