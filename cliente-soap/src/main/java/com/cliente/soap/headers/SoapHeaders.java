package com.cliente.soap.headers;

import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.transport.HeadersAwareSenderWebServiceConnection;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;

public class SoapHeaders implements WebServiceMessageCallback {
	
	@Override
    public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
        TransportContext context = TransportContextHolder.getTransportContext();
        HeadersAwareSenderWebServiceConnection connection = (HeadersAwareSenderWebServiceConnection) context
                .getConnection();
        connection.addRequestHeader("Content-Type", "text/xml");
    }

	

}
