package com.cliente.soap.Entities;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResolveIPResult implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String Country;
	private String Latitude;
	private String Longitude;
	private String AreaCode;
	private String HasDaylightSavings;
	private String Certainty;
	private String CountryCode;


}
