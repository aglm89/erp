package com.cliente.soap.Entities;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestResolveIp implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String ipAddress;
	
	private String licenseKey;

}
